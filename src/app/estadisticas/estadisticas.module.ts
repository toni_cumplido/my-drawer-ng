import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';

import { MyStatsComponent } from '../my-stats/my-stats.component';
import { MyGraficComponent } from '../my-grafic/my-grafic.component';
import { EstadisticasRoutingModule } from './estadisticas-routing.module';

@NgModule({
  declarations: [
    MyStatsComponent,
    MyGraficComponent
  ],
  imports: [
    NativeScriptCommonModule,
    EstadisticasRoutingModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class EstadisticasModule { }
