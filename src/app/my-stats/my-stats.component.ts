import { Component, OnInit } from '@angular/core';
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { RouterExtensions } from 'nativescript-angular/router';
import { Router } from '@angular/router';
import { isAndroid, isIOS, device } from "@nativescript/core/platform"


@Component({
  selector: 'ns-my-stats',
  templateUrl: './my-stats.component.html',
  styleUrls: ['./my-stats.component.css']
})
export class MyStatsComponent implements OnInit {
  private solo_visible_en_android: string = "inicializada";

  constructor(private router: Router, private routerExtensions: RouterExtensions) { 
    
    if (isAndroid) { 
      this.solo_visible_en_android="android";
    }
  }

  ngOnInit(): void {
  }

  getSoloVisibleEnAndroid(): String{
    return this.solo_visible_en_android;
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

  onStatsButtonTap(): void {
    this.routerExtensions.navigate(["/estadisticas/grafic"], {
      transition: {
          name: "fade"
      }
    });

    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.closeDrawer();
  }

}

