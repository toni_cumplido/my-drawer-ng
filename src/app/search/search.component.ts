import { Component, ElementRef, OnInit, ViewChild  } from "@angular/core";
import * as Toast from 'nativescript-toasts'
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "../domain/noticias.service";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { Color } from "tns-core-modules/color";
import { View } from "tns-core-modules/ui/core/view";
import { AnimationCurve } from 'tns-core-modules/ui/enums';
import { ActivityIndicator } from "tns-core-modules/ui/activity-indicator";


@Component({
    selector: "Search",
    templateUrl: "./search.component.html"/*,
    providers: [NoticiasService]*/
})
export class SearchComponent implements OnInit {

    resultados: Array<string>;
    @ViewChild("layout", { static: false }) layout: ElementRef;
    @ViewChild("icono", { static: false }) icono: ElementRef;

    constructor(private noticias: NoticiasService, private router: Router, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        this.noticias.agregar("hola 1!");
        this.noticias.agregar("hola 2!");
        this.noticias.agregar("hola 3!");
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x): void {
        console.dir(x);
        this.routerExtensions.navigate(["/search/detalle"], {
            transition: {
                name: "fade"
            }
        });

        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();

    }

    doLater(fn) { setTimeout(fn, 1000); }

    onPull(e) {
        console.log(e);
        const pullRefresh = e.object;
        setTimeout(() => {
            this.noticias.agregar("hola "+ (this.noticias.sizeof()+1) + "!");
            pullRefresh.refreshing=false;
        },2000);
        const toastOptions: Toast.ToastOptions = {text: "Añadida noticia!", duration: Toast.DURATION.SHORT};
        this.doLater(() => Toast.show(toastOptions));

    }

    buscarAhora(s: string){
        this.resultados = this.noticias.buscar().filter((x) => x.indexOf(s) >= 0);

        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 300,
            delay: 150
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 300,
            delay: 150            
        }));       

        const icono=<View>this.icono.nativeElement;
        icono.animate({
            rotate: 360,
            curve: AnimationCurve.easeInOut,
            duration: 3000
        }).then(() => icono.animate ({
            // Reset animation
            rotate:0
        }));
    }

    cambio_busy (e) {
        let indicator = <ActivityIndicator>e.object;
        console.log("indicator.busy: " + indicator.busy);
    }
}
