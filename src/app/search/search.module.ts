import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { SearchFormComponent } from "./search-form.component";
import { SearchRoutingModule } from "./search-routing.module";
import { SearchComponent } from "./search.component";
import { DetalleComponent } from "../detalle/detalle.component";

import { MinLenDirective } from "../domain/minlen.validator"; 
/*import { NoticiasService } from "../domain/noticias.service";*/

@NgModule({
    imports: [
        NativeScriptCommonModule,
        SearchRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        SearchComponent,
        DetalleComponent,
        SearchFormComponent,
        MinLenDirective
    ],
    /*providers: [NoticiasService],*/
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SearchModule { }
